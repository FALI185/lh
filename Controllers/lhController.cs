﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ListHell.Controllers
{
    public class lhController : Controller
    {

        CODE.DAL dal;
        public lhController()
        {
            dal = new CODE.DAL();
            
        }
        // GET: lh
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> area()
        {
            List<CODE.Area> v =await dal.getAreas();
            return View(v);
        }
        public async Task<ActionResult> cities(string area)
        {
            List<CODE.Locations> v = await dal.getCities(area);
            return View(v);
        }

        public async Task<ActionResult> categories(int lid)
        {
            List<CODE.Categories> v = await dal.getCategories(lid);
            return View(v);
                 
        }

       
        public async Task<ActionResult> ads(int lid,int catid,int pn=0)
        {
            List<CODE.AdsModel> v = await dal.getAds(lid,catid,pn);
            int x =await dal.getCount(lid, catid);
            ViewBag.paging = x+","+pn;
            ViewBag.ct = catid.ToString();
            ViewBag.lid = lid.ToString();
            return View(v);
        }
        public async Task<ActionResult> getAutosDetails()
        {
            return PartialView("_Autos");
        }
        [HttpGet]
        public async Task<ActionResult> Post()
        {
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                   // DoSomethingWith(error);
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Post(int x)
        {
            return View();
        }
    }
}