﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ListHell.CODE
{
    public class DAL
    {
        LH_newEntities context;
        public DAL()
        {
            context = new LH_newEntities();
        }
        public async Task<List<Area>> getAreas()
        {
            var v = await context.Locations.DistinctBy(w => w.area).Select(w => new Area { Areas = w.area }).ToListAsync();
            return v;

        }
        public async Task<List<Locations>> getCities(string area)
        {

            List<CODE.Locations> v = await context.Locations.Where(w => w.area == area)
                                    .Select(w => new CODE.Locations { area = w.area, city = w.city, id = w.id,description=context.ads.Where(x=>x.lid==w.id).Count().ToString() })
                                    .OrderBy(w=>w.city)
                                    .ToListAsync();
            return v;
        }
        public async Task<List<Categories>> getCategories(int lid) {
            List<CODE.Categories> v = await context.Categories.Select(w => new CODE.Categories
            {
                category1 = w.category1,
                id=w.id,
                master_category_icon=w.master_category_icon,
                master_category=w.master_category,
                master_category_descr=w.master_category_descr,
                lid=w.lid
                


            }).Where(w=>w.lid==lid).OrderBy(w=>w.master_category).ToListAsync();
            return v;

        }
        public async Task<int> getCount(int lid,int catid)
        {
            int v = await context.ads.Where(w => w.catid == catid && w.lid == lid).CountAsync();

            return v;
        }
        public async Task<List<AdsModel>> getAds(int lid,int catid,int pn=0) {
            var v = await context.ads.Select(w => new
            {
                a = w

            }).Join(context.Categories, a => a.a.catid, b => b.id, (a, b) => new { x = a, y = b }).Where(w => w.x.a.catid == catid)
              .Join(context.Locations, a => a.x.a.lid, b => b.id, (a, b) => new
              {
                  adid = a.x.a.adid,
                  address = a.x.a.address,  
                  phone = a.x.a.phone,
                  title = a.x.a.title,
                  catname = b.city,
                  areaname = b.area,
                  catid = a.x.a.catid,
                  lid = a.x.a.lid,
                  locname = b.state_province,
                  dt = a.x.a.datetime,
                  amount = a.x.a.amount,
                  descr = a.x.a.description,
                  cat=a.y.category1,
                  mastercat=a.y.master_category,
                  catdescr=a.y.category_descr
                 

              }).Where(w => w.lid == lid)
              .GroupJoin(context.Images, a => a.adid, b => b.adid, (a, b) => new {
                  x=a,
                  y = b.Where(f => f.defaulti == true) })
                .Select(w => new AdsModel
                 {
                     adid = w.x.adid,
                     amount = w.x.amount,
                     lid = w.x.lid,
                     title = w.x.title,
                     dt = w.x.dt,
                     src = w.y.FirstOrDefault().src,
                     imageid = w.y.FirstOrDefault().imageid,
                     cat=w.x.cat,
                     catdescr=w.x.catdescr,
                     mastercat=w.x.mastercat,
                     descr=w.x.descr,
                     phone=w.x.phone

                 })

              .OrderByDescending(w => w.dt).Skip(pn * 10).Take(10).ToListAsync();
            return v;
        }
      
    }
}